 void sad_block_8x8_sse_1_2(uint8_t *block1, uint8_t *block2, int stride, int *result)
{
  register int res =0; 
  int stride_r = stride; //sparer ca 10 (401) ms, hvis gjort til reg, taper 3-4 (404) ms i forhold til dette 
  __m128i sum;


  //uint8_t t[64]  __attribute__ ((aligned (16)));
  //uint8_t t2[64]  __attribute__ ((aligned (16)));
  
  uint16_t get_res[8]  __attribute__ ((aligned (16)));
  uint8_t t[64]  __attribute__ ((aligned (16))),t2[64]  __attribute__ ((aligned (16)));
  /*
  int to,tre,fire,fem,seks,syv;
  
  to = stride_r*2; tre = stride_r*3; fire = stride_r*4; fem = stride_r*5; seks = stride_r*6;
  syv = stride_r*7;
  _mm_prefetch(block2,1);
  //160 ms
  //_mm_prefetch(block1+stride_r,1);
  memcpy(&t[0],&block1[0],8); 
  //_mm_prefetch(&block1[to],1);
  memcpy(&t[8],&block1[stride_r],8);
  //_mm_prefetch(&block1[tre],1);
  memcpy(&t[16],&block1[to],8); 
  //_mm_prefetch(&block1[fire],1);
  memcpy(&t[24],&block1[tre],8);
  //_mm_prefetch(&block1[fem],1);
  memcpy(&t[32],&block1[fire],8);
  //_mm_prefetch(&block1[seks],1);
  memcpy(&t[40],&block1[fem],8);
  //_mm_prefetch(&block1[syv],1);
  memcpy(&t[48],&block1[seks],8);
  _mm_prefetch(block2,1);
  memcpy(&t[56],&block1[syv],8);

  _mm_prefetch(block2 + stride_r,1);
  memcpy(&t2[0],&block2[0],8); 
  //_mm_prefetch(&block2[to],1);
  memcpy(&t2[8],&block2[stride_r],8);
  //_mm_prefetch(&block2[tre],1);
  memcpy(&t2[16],&block2[to],8);
  //_mm_prefetch(&block2[fire],1);
  memcpy(&t2[24],&block2[tre],8);
  // _mm_prefetch(&block2[fem],1);
  memcpy(&t2[32],&block2[fire],8); 
  //_mm_prefetch(&block2[seks],1);
  memcpy(&t2[40],&block2[fem],8);
  //_mm_prefetch(&block2[syv],1);
  memcpy(&t2[48],&block2[seks],8); 
  memcpy(&t2[56],&block2[syv],8);
  
  */  
  
  //ca +177ms
  memcpy(&t[0],&block1[0],8); memcpy(&t[8],&block1[stride_r],8);
  memcpy(&t[16],&block1[stride_r*2],8); memcpy(&t[24],&block1[stride_r*3],8);
  memcpy(&t[32],&block1[stride_r*4],8); memcpy(&t[40],&block1[stride_r*5],8);
  memcpy(&t[48],&block1[stride_r*6],8); memcpy(&t[56],&block1[stride_r*7],8);
  _mm_prefetch(&t[0],1); //ca -4 ms
  memcpy(&t2[0],&block2[0],8); memcpy(&t2[8],&block2[stride_r],8);
  memcpy(&t2[16],&block2[stride_r*2],8); memcpy(&t2[24],&block2[stride_r*3],8);
  memcpy(&t2[32],&block2[stride_r*4],8); memcpy(&t2[40],&block2[stride_r*5],8);
  memcpy(&t2[48],&block2[stride_r*6],8); memcpy(&t2[56],&block2[stride_r*7],8);
  //_mm_prefetch(&t2[0],1);
  
  
  
  sum = _mm_sad_epu8( _mm_load_si128(&t[0]),_mm_load_si128(&t2[0]));
  sum = _mm_adds_epu16(sum,_mm_sad_epu8(_mm_load_si128(&t[16]),_mm_load_si128(&t2[16]))); 
  
  sum = _mm_adds_epu16(sum,_mm_sad_epu8(_mm_load_si128(&t[32]),_mm_load_si128(&t2[32])));
  sum = _mm_adds_epu16(sum,_mm_sad_epu8( _mm_load_si128(&t[48]),_mm_load_si128(&t2[48])));

  
  _mm_store_si128(&get_res[0],sum);
  

  res += get_res[0]; 
  res += get_res[4];

 
  *result = res;
  /*
  
  int re;
  //Test
  sad_block_8x8_old(block1,block2,stride,&re);
  if(re != *result) {
  printf("BUG in sad_block re:%d  result:%d\n",re,*result);
  
  }
  */
}




