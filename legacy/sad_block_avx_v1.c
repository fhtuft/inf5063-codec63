#ifdef __AVX__
void sad_block_8x8_avx_v1(uint8_t *block1, uint8_t *block2, int stride, int *result){
  START_OF_CALL(sad_block_8x8);
  register int res =0; 
  int stride_r = stride; //sparer ca 10 (401) ms, hvis gjort til reg, taper 3-4 (404) ms i forhold til dette 

  __m256i sum;

  
  uint16_t get_res[16]  __attribute__ ((aligned (32)));
  uint64_t dirty_hack1[8]  __attribute__ ((aligned (32))), dirty_hack2[8]  __attribute__ ((aligned (32)));

  
    // +40 ms
  dirty_hack1[0] =((uint64_t*)&block1[0])[0]; dirty_hack1[1] =((uint64_t*)&block1[stride_r])[0];
  dirty_hack1[2] =((uint64_t*)&block1[stride_r*2])[0]; dirty_hack1[3] =((uint64_t*)&block1[stride_r*3])[0];
  dirty_hack1[4] =((uint64_t*)&block1[stride_r*4])[0]; dirty_hack1[5] =((uint64_t*)&block1[stride_r*5])[0];
  dirty_hack1[6] =((uint64_t*)&block1[stride_r*6])[0]; dirty_hack1[7] =((uint64_t*)&block1[stride_r*7])[0];
  
  dirty_hack2[0] =((uint64_t*)&block2[0])[0]; dirty_hack2[1] =((uint64_t*)&block2[stride_r])[0];
  dirty_hack2[2] =((uint64_t*)&block2[stride_r*2])[0]; dirty_hack2[3] =((uint64_t*)&block2[stride_r*3])[0];
  dirty_hack2[4] =((uint64_t*)&block2[stride_r*4])[0]; dirty_hack2[5] =((uint64_t*)&block2[stride_r*5])[0];
  dirty_hack2[6] =((uint64_t*)&block2[stride_r*6])[0]; dirty_hack2[7] =((uint64_t*)&block2[stride_r*7])[0];
 
  
 																      
 
  sum = _mm256_sad_epu8( _mm256_load_si256(&dirty_hack1[0]),_mm256_load_si256(&dirty_hack2[0]));
  sum = _mm256_adds_epu16(sum,_mm256_sad_epu8( _mm256_load_si256(&dirty_hack1[4]),_mm256_load_si256(&dirty_hack2[4])));
  

 
  _mm256_store_si256(&get_res[0],sum);
  
 
  
  res += get_res[0]; 
  res += get_res[4]; 
  res += get_res[8]; 
  res += get_res[12];
  
  *result = res;
  END_OF_CALL();
  
  /*
  int re;
  //Test
  sad_block_8x8_old(block1,block2,stride,&re);
  if(re != *result) {
  printf("BUG in sad_block re:%d  result:%d\n",re,*result);
  }
  */
}
#endif /* __AVX__ */

