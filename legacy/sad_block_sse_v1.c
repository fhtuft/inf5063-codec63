/*Forste vektor kode av sad.. 


*/


//Signde Or Unsigned char
void sad_block_8x8_sse_v1(uint8_t *block1, uint8_t *block2, int stride, int *result)
{
  register int res =0;

  __m128i a,b,c,d,res1,min_v,max_v;
  
  uint8_t t[64]  __attribute__ ((aligned (16)));
  uint8_t t2[64]  __attribute__ ((aligned (16)));
  
 
  //160 ms
  memcpy(&t[0],&block1[0],8); memcpy(&t[8],&block1[stride],8);
  memcpy(&t[16],&block1[stride*2],8); memcpy(&t[24],&block1[stride*3],8);
  memcpy(&t[32],&block1[stride*4],8); memcpy(&t[40],&block1[stride*5],8);
  memcpy(&t[48],&block1[stride*6],8); memcpy(&t[56],&block1[stride*7],8);

  memcpy(&t2[0],&block2[0],8); memcpy(&t2[8],&block2[stride],8);
  memcpy(&t2[16],&block2[stride*2],8); memcpy(&t2[24],&block2[stride*3],8);
  memcpy(&t2[32],&block2[stride*4],8); memcpy(&t2[40],&block2[stride*5],8);
  memcpy(&t2[48],&block2[stride*6],8); memcpy(&t2[56],&block2[stride*7],8);
   

  a = _mm_load_si128(&t[0]); 
  b = _mm_load_si128(&t2[0]);
  c = _mm_load_si128(&t[16]);
  d = _mm_load_si128(&t2[16]);


  max_v =_mm_max_epu8(a,b);min_v= _mm_min_epu8(a,b);
  res1=  _mm_subs_epu8(max_v,min_v);   
  _mm_store_si128(&t[0],res1);
 
  res += t[0]; res += t[1];res += t[2];res += t[3];res += t[4];res += t[5];
  res += t[6]; res += t[7];res += t[8];res += t[9];res += t[10];res += t[11];
  res += t[12]; res += t[13]; res += t[14]; res += t[15];


  max_v =_mm_max_epu8(d,c);min_v= _mm_min_epu8(d,c);
  res1 = _mm_subs_epu8(max_v,min_v);
  _mm_store_si128(&t[0],res1);
 
  res += t[0]; res += t[1];res += t[2];res += t[3];res += t[4];res += t[5];
  res += t[6]; res += t[7];res += t[8];res += t[9];res += t[10];res += t[11];
  res += t[12]; res += t[13]; res += t[14]; res += t[15];
 

  a = _mm_load_si128(&t[32]); 
  b = _mm_load_si128(&t2[32]);
  c = _mm_load_si128(&t[48]); 
  d = _mm_load_si128(&t2[48]);
 

  max_v =_mm_max_epu8(a,b);min_v= _mm_min_epu8(a,b);
  res1 = _mm_subs_epu8(max_v,min_v);
  _mm_store_si128(&t[0],res1);

  res += t[0]; res += t[1];res += t[2];res += t[3];res += t[4];res += t[5];
  res += t[6]; res += t[7];res += t[8];res += t[9];res += t[10];res += t[11];
  res += t[12]; res += t[13]; res += t[14]; res += t[15];


  max_v =_mm_max_epu8(c,d);min_v= _mm_min_epu8(c,d);
  res1 = _mm_subs_epu8(max_v,min_v);
  _mm_store_si128(&t[0],res1);

  res += t[0]; res += t[1];res += t[2];res += t[3];res += t[4];res += t[5];
  res += t[6]; res += t[7];res += t[8];res += t[9];res += t[10];res += t[11];
  res += t[12]; res += t[13]; res += t[14]; res += t[15];
  
  
  *result = res;

  /*
  int re;
  //Test
  sad_block_8x8_old(block1,block2,stride,&re);
  if(re != *result) {
  printf("BUG in sad_block re:%d  result:%d\n",re,*result);
  
  }
  */
}
