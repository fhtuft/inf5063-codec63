//Signde Or Unsigned char
void sad_block_8x8_sse_1_1(uint8_t *block1, uint8_t *block2, int stride, int *result)
{
    START_OF_CALL(sad_block_8x8);
  register int res =0; 
  int stride_r = stride; //sparer ca 10 (401) ms, hvis gjort til reg, taper 3-4 (404) ms i forhold til dette 
  __m128i a,b,c,d,res1,res2,res3,min_v,max_v,sum;
  __m128i zero_vec    = _mm_setzero_si128();


  uint8_t t[64]  __attribute__ ((aligned (16)));
  uint8_t t2[64]  __attribute__ ((aligned (16)));
  
  uint16_t get_res[8]  __attribute__ ((aligned (16)));
  //uint32_t multi_c[4] = {4,5,6,7};

  //int mem_addr[6];
  /*
  mem_addr[0] = stride*2;  mem_addr[1] = stride*3; mem_addr[2] = stride*4;  mem_addr[3] = stride*5;
  mem_addr[5] = stride*6;  mem_addr[6] = stride*7;
  */
  /*
  memcpy(&t[0],&block1[0],8); memcpy(&t[8],&block1[stride],8);
  memcpy(&t[16],&block1[mem_addr[0]],8); memcpy(&t[24],&block1[mem_addr[1]],8);
  memcpy(&t[32],&block1[mem_addr[2]],8); memcpy(&t[40],&block1[mem_addr[3]],8);
  memcpy(&t[48],&block1[mem_addr[4]],8); memcpy(&t[56],&block1[mem_addr[5]],8);

  
  memcpy(&t2[0],&block2[0],8); memcpy(&t2[8],&block2[stride],8);
  memcpy(&t2[16],&block2[mem_addr[0]],8); memcpy(&t2[24],&block2[mem_addr[1]],8);
  memcpy(&t2[32],&block2[mem_addr[2]],8); memcpy(&t2[40],&block2[mem_addr[3]],8);
  memcpy(&t2[48],&block2[mem_addr[4]],8); memcpy(&t2[56],&block2[mem_addr[5]],8);
  */
  
  
  //160 ms
  memcpy(&t[0],&block1[0],8); memcpy(&t[8],&block1[stride_r],8);
  memcpy(&t[16],&block1[stride_r*2],8); memcpy(&t[24],&block1[stride_r*3],8);
  memcpy(&t[32],&block1[stride_r*4],8); memcpy(&t[40],&block1[stride_r*5],8);
  memcpy(&t[48],&block1[stride_r*6],8); memcpy(&t[56],&block1[stride_r*7],8);

  memcpy(&t2[0],&block2[0],8); memcpy(&t2[8],&block2[stride_r],8);
  memcpy(&t2[16],&block2[stride_r*2],8); memcpy(&t2[24],&block2[stride_r*3],8);
  memcpy(&t2[32],&block2[stride_r*4],8); memcpy(&t2[40],&block2[stride_r*5],8);
  memcpy(&t2[48],&block2[stride_r*6],8); memcpy(&t2[56],&block2[stride_r*7],8);
  

  /*
 //160 ms
  memcpy(&t[0],&block1[0],8); memcpy(&t[8],&block1[stride],8);
  memcpy(&t[16],&block1[stride*2],8); memcpy(&t[24],&block1[stride*3],8);
  memcpy(&t[32],&block1[stride*4],8); memcpy(&t[40],&block1[stride*5],8);
  memcpy(&t[48],&block1[stride*6],8); memcpy(&t[56],&block1[stride*7],8);

  memcpy(&t2[0],&block2[0],8); memcpy(&t2[8],&block2[stride],8);
  memcpy(&t2[16],&block2[stride*2],8); memcpy(&t2[24],&block2[stride*3],8);
  memcpy(&t2[32],&block2[stride*4],8); memcpy(&t2[40],&block2[stride*5],8);
  memcpy(&t2[48],&block2[stride*6],8); memcpy(&t2[56],&block2[stride*7],8);
  */
  
  a = _mm_load_si128(&t[0]); 
  b = _mm_load_si128(&t2[0]);
  c = _mm_load_si128(&t[16]);
  d = _mm_load_si128(&t2[16]);


  max_v =_mm_max_epu8(a,b);min_v= _mm_min_epu8(a,b);
  res1=  _mm_subs_epu8(max_v,min_v);   
  res3 =_mm_unpacklo_epi8 (res1,zero_vec);
  res2 = _mm_unpackhi_epi8(res1,zero_vec);
  sum = _mm_adds_epu16(res3,res2);



  max_v =_mm_max_epu8(d,c);min_v= _mm_min_epu8(d,c);
  res1 = _mm_subs_epu8(max_v,min_v);
  res3 =_mm_unpacklo_epi8 (res1,zero_vec);
  res2 = _mm_unpackhi_epi8(res1,zero_vec);
  sum = _mm_adds_epu16(res3,sum);
  sum = _mm_adds_epu16(sum,res2);

  a = _mm_load_si128(&t[32]); 
  b = _mm_load_si128(&t2[32]);
  c = _mm_load_si128(&t[48]); 
  d = _mm_load_si128(&t2[48]);
 

  max_v =_mm_max_epu8(a,b);min_v= _mm_min_epu8(a,b);
  res1 = _mm_subs_epu8(max_v,min_v);
  res3 =_mm_unpacklo_epi8 (res1,zero_vec);
  res2 = _mm_unpackhi_epi8(res1,zero_vec);
  sum = _mm_adds_epu16(res3,sum);
  sum = _mm_adds_epu16(sum,res2);


  max_v =_mm_max_epu8(c,d);min_v= _mm_min_epu8(c,d);
  res1 = _mm_subs_epu8(max_v,min_v);
  res3 =_mm_unpacklo_epi8 (res1,zero_vec);
  res2 = _mm_unpackhi_epi8(res1,zero_vec);
  sum = _mm_adds_epu16(res3,sum);
  sum = _mm_adds_epu16(sum,res2);
  
  
  _mm_store_si128(&get_res[0],sum);
  

  res += get_res[0]; res += get_res[1];res += get_res[2];res += get_res[3];
  res += get_res[4];res += get_res[5]; res += get_res[6]; res += get_res[7];

 
  *result = res;

  END_OF_CALL();
  /*
  int re;
  //Test
  sad_block_8x8_old(block1,block2,stride,&re);
  if(re != *result) {
  printf("BUG in sad_block re:%d  result:%d\n",re,*result);
  
  }
  */
}

