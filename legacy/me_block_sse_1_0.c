//me_block sse 60 ms!!

static void me_block_8x8_sse_1_0(struct c63_common *cm, int mb_x, int mb_y,
    uint8_t *orig, uint8_t *ref, int color_component)
{
  struct macroblock *mb =
    &cm->curframe->mbs[color_component][mb_y*cm->padw[color_component]/8+mb_x];

  int range = cm->me_search_range;

  /* Quarter resolution for chroma channels. */
  if (color_component > 0) { range /= 2; }

  int left = mb_x * 8 - range;
  int top = mb_y * 8 - range;
  int right = mb_x * 8 + range;
  int bottom = mb_y * 8 + range;

  int w = cm->padw[color_component];
  int h = cm->padh[color_component];

  /* Make sure we are within bounds of reference frame. TODO: Support partial
     frame bounds. */
  if (left < 0) { left = 0; }
  if (top < 0) { top = 0; }
  if (right > (w - 8)) { right = w; }
  if (bottom > (h - 8)) { bottom = h; }

  int x, y;

  int mx = mb_x * 8;
  int my = mb_y * 8;
  

  int best_sad = INT_MAX;

  //printf("left right:  %d  %d\n",left,right);
  /*
    antar at left - right kan deles på 8
    hvis ikke må resterende funksjoner kall gjøres under x loop

  */
  uint8_t *block1, *block2;
  uint64_t dirty_hack1[8]  __attribute__ ((aligned (32))), dirty_hack2[8]  __attribute__ ((aligned (32)));
  
  //__m256i sum,sum2;
  __m128i sum;
  
  uint16_t get_res[16]  __attribute__ ((aligned (32)));


  int stride_r = w;
  register int res =0;

  block1 = orig + my*w+mx;

  dirty_hack1[0] =((uint64_t*)&block1[0])[0]; dirty_hack1[1] =((uint64_t*)&block1[stride_r])[0];
  dirty_hack1[2] =((uint64_t*)&block1[stride_r*2])[0]; dirty_hack1[3] =((uint64_t*)&block1[stride_r*3])[0];
  dirty_hack1[4] =((uint64_t*)&block1[stride_r*4])[0]; dirty_hack1[5] =((uint64_t*)&block1[stride_r*5])[0];
  dirty_hack1[6] =((uint64_t*)&block1[stride_r*6])[0]; dirty_hack1[7] =((uint64_t*)&block1[stride_r*7])[0];
  
  
  for (y = top; y < bottom; ++y)
  {
    
    //loop unrolling med 4: 68-69ms med 8: 69ms 4 bedre.. Men sparer bare 1 ms på dette...
    for (x = left; x < right; x++)
    {
      

      //Start BLOK1
      res =0;
      block2 = ref + y*w + x;
      
      //__builtin_prefetch(ref + (y)*w+x,2);
      dirty_hack2[0] =((uint64_t*)&block2[0])[0]; dirty_hack2[1] =((uint64_t*)&block2[stride_r])[0];
      dirty_hack2[2] =((uint64_t*)&block2[stride_r*2])[0]; dirty_hack2[3] =((uint64_t*)&block2[stride_r*3])[0];
      dirty_hack2[4] =((uint64_t*)&block2[stride_r*4])[0]; dirty_hack2[5] =((uint64_t*)&block2[stride_r*5])[0];
      dirty_hack2[6] =((uint64_t*)&block2[stride_r*6])[0]; dirty_hack2[7] =((uint64_t*)&block2[stride_r*7])[0];

      //sum = _mm256_sad_epu8( _mm256_lddqu_si256((__m256i*)&dirty_hack1[0]),_mm256_lddqu_si256((__m256i*)&dirty_hack2[0]));
      //sum = _mm256_adds_epu16(sum,_mm256_sad_epu8( _mm256_lddqu_si256((__m256i*)&dirty_hack1[4]),_mm256_lddqu_si256((__m256i*)&dirty_hack2[4])));
      
      //prøvde denne med legger til +10 ms 
      //sum = _mm256_adds_epu16(sum,_mm256_unpackhi_epi16(sum,sum));
      
       sum = _mm_sad_epu8( _mm_loadu_si128((__m128i*)&dirty_hack1[0]),_mm_loadu_si128((__m128i*)&dirty_hack2[0]));
  sum = _mm_adds_epu16(sum,_mm_sad_epu8(_mm_loadu_si128((__m128i*)&dirty_hack1[2]),_mm_loadu_si128((__m128i*)&dirty_hack2[2]))); 
  sum = _mm_adds_epu16(sum,_mm_sad_epu8(_mm_loadu_si128((__m128i*)&dirty_hack1[4]),_mm_loadu_si128((__m128i*)&dirty_hack2[4])));
  sum = _mm_adds_epu16(sum,_mm_sad_epu8( _mm_loadu_si128((__m128i*)&dirty_hack1[6]),_mm_loadu_si128((__m128i*)&dirty_hack2[6])));
  
      
  
     /*
     writeout
     */

      //+10ms
      //_mm256_store_si256((__m256i*)&get_res[0],sum);
  _mm_store_si128((__m128i*)&get_res[0],sum);

      //_mm_store_si128((__m128i*)&get_res[0],(__m128)sum);
      //+6ms
      res += get_res[0]; 
      res += get_res[4]; 
      //res += get_res[8]; 
      //res += get_res[12];
  
      //*result = res;

      if (res < best_sad)
      {
        mb->mv_x = x - mx;
        mb->mv_y = y - my;
        best_sad = res;
      }
      
      //END BLOK1
      

     
      int sad;
       
      sad_block_8x8(orig + my*w+mx, ref + y*w+x, w, &sad);
      if(sad != res) {
	printf("me_block: %d  %d",sad,res);

      }
      

      /* printf("(%4d,%4d) - %d\n", x, y, sad); */
      
    }
  }

  /* Here, there should be a threshold on SAD that checks if the motion vector
     is cheaper than intraprediction. We always assume MV to be beneficial */

  /* printf("Using motion vector (%d, %d) with SAD %d\n", mb->mv_x, mb->mv_y,
     best_sad); */

  mb->use_mv = 1;
}
