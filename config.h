#ifndef CONFIG_H_
#define CONFIG_H_

/*
Possible old versions of functions:
sad_block_sse_v1
sad_block_sse_1_1
sad_block_sse_1_2

sad_block_avx_v1

transpose_block_sse_1_0
*/
#define dct_1d                  dct_1d_sse
#define idct_1d                 idct_1d_sse
#define transpose_block         transpose_block_sse
#define scale_block             scale_block_sse
#define quantize_block          quantize_block_old
#define dequantize_block        dequantize_block
#define dct_quant_block_8x8     dct_quant_block_8x8
#define dequant_idct_block_8x8  dequant_idct_block_8x8
#define sad_block_8x8           sad_block_8x8_avx

/*
Possible old versions of functions:
me_block_avx_1_0
me_block_sse_1_0
*/
#define me_block_8x8_sse        me_block_8x8

#endif /* CONFIG_H_ */
