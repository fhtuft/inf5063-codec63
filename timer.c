#if defined(MONITOR_TIME) &&  MONITOR_TIME==1
#include <stdio.h>
#include <string.h>
#include <assert.h>
#include "timer.h"

#define FUNCTION_TO_TIME(x) struct TimerStruct x##_time;
#include "functions_to_time.x"
#undef FUNCTION_TO_TIME

// Read RDTSC register - clock cycle counter
static __inline__ unsigned long long RDTSC(void)
{
#if defined(__i386__)
    unsigned long long int retval;
    __asm__ volatile (".byte 0x0f, 0x31" : "=A" (retval));
    return retval;
#elif defined(__x86_64__)
    unsigned hi, lo;
    __asm__ volatile (".byte 0x0f, 0x31" : "=a" (lo), "=d" (hi));
    return ((unsigned long long)lo) | (((unsigned long long)hi) << 32);
#else
# error missing RDTSC code
#endif
}

struct TimerStruct *callStack[20];
/* Set to -1 so init_timer can use it as test that it has not been called
 * before. After init_timer it should invariantly be <= 0 */
size_t stack_ix = -1; 
struct TimerStruct totalTime;

void init_timer(void){
    /* Make sure this function has not been called before */
    assert(stack_ix == -1);

#define FUNCTION_TO_TIME(x) memset(&x##_time,0,sizeof(struct TimerStruct));
#include "functions_to_time.x"
#undef FUNCTION_TO_TIME

    stack_ix = 0;
    callStack[stack_ix] = &totalTime;
    totalTime.effectiveTime = 0;
    totalTime.callTime = 0;
    totalTime.functionStart = RDTSC();
}

void print_times(void){
    /* Make sure this function is not called from inside any of the functions
     * that is timed */
    assert(stack_ix == 0);

    unsigned long long rdtsc = RDTSC();
    unsigned long long delta = rdtsc - totalTime.functionStart;
    totalTime.effectiveTime += delta;
    totalTime.callTime += delta;

    FILE *file = stdout;
    fprintf(file,"%-24s%-18s%-17s\n","Function:", "Self time:", 
            "Cumulative time:");
#define FUNCTION_TO_TIME(x) fprintf(file,"%-24s%17llu%17llu\n", #x, \
        x##_time.effectiveTime, x##_time.callTime);
#include "functions_to_time.x"
#undef FUNCTION_TO_TIME 
    fprintf(file,"%-24s%17llu%17llu","Total Time:", totalTime.effectiveTime,
            totalTime.callTime);
    
    /* Restart timer */
    totalTime.functionStart = RDTSC();
}


__inline__ void start_of_call(struct TimerStruct *ts){
    /* Get the time as soon as possible */
    unsigned long long rdtsc = RDTSC();

    /* Update the caller */
    struct TimerStruct *caller_ts = callStack[stack_ix];
    unsigned long long delta = rdtsc - caller_ts->functionStart;
    caller_ts->effectiveTime += delta;
    caller_ts->callTime += delta;

    /* Push the called function to the call stack */
    callStack[++stack_ix] = ts;

    /* Start the timer for the called function */
    ts->functionStart = RDTSC();
}

__inline__ void end_of_call(void){
    /* Get the time as soon as possible */
    unsigned long long rdtsc = RDTSC();

    /* Pop current function from stack */
    struct TimerStruct *ts = callStack[stack_ix--]; 

    /* Update the time the function has used */
    unsigned long long delta = rdtsc - ts->functionStart;
    ts->effectiveTime += delta;
    ts->callTime += delta;
    
    /* update the caller */
    struct TimerStruct *caller_ts = callStack[stack_ix];
    caller_ts->callTime += delta;

    /* Restart timer of the caller */
    caller_ts->functionStart = RDTSC();
}
#endif
