For å bygge koden:
make all

For å encode foreman.yuv:
make foreman
Resultatet blir lagret i foreman.c63

For å encode <filename>.yuv:
make foreman FILE=<filename>
Resultatet blir lagret i <filename>.c63
