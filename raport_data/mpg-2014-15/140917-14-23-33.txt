140917-14-23-33

#define dct_1d                  dct_1d_sse
#define idct_1d                 idct_1d_sse
#define transpose_block         transpose_block_old
#define scale_block             scale_block_old
#define quantize_block          quantize_block_old
#define sad_block_8x8           sad_block_8x8_avx_v1
#define me_block_8x8_old        me_block_8x8

make[1]: Entering directory `/home/inf5063-g05/exam1/inf5063-codec63'
#./c63enc -w 352 -h 288 -o foreman -f 10 foreman_cif.yuv
./c63enc -w 352 -h 288 -o foreman.c63 -f 10 foreman.yuv
Limited to 10 frames.
Encoding frame 0, time ms: 28 Done!
Encoding frame 1, time ms: 91 Done!
Encoding frame 2, time ms: 90 Done!
Encoding frame 3, time ms: 91 Done!
Encoding frame 4, time ms: 91 Done!
Encoding frame 5, time ms: 90 Done!
Encoding frame 6, time ms: 91 Done!
Encoding frame 7, time ms: 91 Done!
Encoding frame 8, time ms: 90 Done!
Encoding frame 9, time ms: 91 Done!
make[1]: Leaving directory `/home/inf5063-g05/exam1/inf5063-codec63'

Flat profile:

Each sample counts as 0.01 seconds.
  %   cumulative   self              self     total           
 time   seconds   seconds    calls  ms/call  ms/call  name    
 56.53      0.39     0.39 15158016     0.00     0.00  sad_block_8x8_avx_v1
 10.15      0.46     0.07    23760     0.00     0.00  dequantize_block
  8.70      0.52     0.06    23760     0.00     0.00  quantize_block_old
  7.25      0.57     0.05    21384     0.00     0.02  me_block_8x8
  5.80      0.61     0.04   380160     0.00     0.00  dct_1d_sse
  2.90      0.63     0.02    47520     0.00     0.00  scale_block_old
  1.45      0.64     0.01   380160     0.00     0.00  idct_1d_sse
  1.45      0.65     0.01    95040     0.00     0.00  transpose_block_old
  1.45      0.66     0.01    23760     0.00     0.00  dequant_idct_block_8x8
  1.45      0.67     0.01    23760     0.00     0.00  write_block
  1.45      0.68     0.01      720     0.01     0.17  dct_quantize_row
  1.45      0.69     0.01      720     0.01     0.16  dequantize_idct_row
  0.00      0.69     0.00   490521     0.00     0.00  put_bits
  0.00      0.69     0.00   210651     0.00     0.00  bit_width
  0.00      0.69     0.00   142168     0.00     0.00  put_byte
  0.00      0.69     0.00    23760     0.00     0.00  dct_quant_block_8x8
  0.00      0.69     0.00    21384     0.00     0.00  mc_block_8x8
  0.00      0.69     0.00    11880     0.00     0.00  write_interleaved_data_MCU
  0.00      0.69     0.00      110     0.00     0.00  put_bytes
  0.00      0.69     0.00       40     0.00     0.00  write_DHT_HTS
  0.00      0.69     0.00       30     0.00     4.17  dct_quantize
  0.00      0.69     0.00       30     0.00     3.83  dequantize_idct
  0.00      0.69     0.00       11     0.00     0.00  gettime
  0.00      0.69     0.00       10     0.00    69.01  c63_encode_image
  0.00      0.69     0.00       10     0.00     0.00  create_frame
  0.00      0.69     0.00       10     0.00     0.00  destroy_frame
  0.00      0.69     0.00       10     0.00     0.00  flush_bits
  0.00      0.69     0.00       10     0.00     0.00  get_dt
  0.00      0.69     0.00       10     0.00     0.00  read_yuv
  0.00      0.69     0.00       10     0.00     0.00  write_DHT
  0.00      0.69     0.00       10     0.00     0.00  write_DQT
  0.00      0.69     0.00       10     0.00     0.00  write_EOI
  0.00      0.69     0.00       10     0.00     0.00  write_SOF0
  0.00      0.69     0.00       10     0.00     0.00  write_SOI
  0.00      0.69     0.00       10     0.00     0.00  write_SOS
  0.00      0.69     0.00       10     0.00     1.00  write_frame
  0.00      0.69     0.00       10     0.00     1.00  write_interleaved_data
  0.00      0.69     0.00        9     0.00     0.00  c63_motion_compensate
  0.00      0.69     0.00        9     0.00    48.90  c63_motion_estimate
  0.00      0.69     0.00        1     0.00     0.00  init_c63_enc

 %         the percentage of the total running time of the
time       program used by this function.

cumulative a running sum of the number of seconds accounted
 seconds   for by this function and those listed above it.

 self      the number of seconds accounted for by this
seconds    function alone.  This is the major sort for this
           listing.

calls      the number of times this function was invoked, if
           this function is profiled, else blank.
 
 self      the average number of milliseconds spent in this
ms/call    function per call, if this function is profiled,
	   else blank.

 total     the average number of milliseconds spent in this
ms/call    function and its descendents per call, if this 
	   function is profiled, else blank.

name       the name of the function.  This is the minor sort
           for this listing. The index shows the location of
	   the function in the gprof listing. If the index is
	   in parenthesis it shows where it would appear in
	   the gprof listing if it were to be printed.

Copyright (C) 2012 Free Software Foundation, Inc.

Copying and distribution of this file, with or without modification,
are permitted in any medium without royalty provided the copyright
notice and this notice are preserved.

		     Call graph (explanation follows)


granularity: each sample hit covers 2 byte(s) for 1.45% of 0.69 seconds

index % time    self  children    called     name
                0.00    0.69      10/10          main [2]
[1]    100.0    0.00    0.69      10         c63_encode_image [1]
                0.00    0.44       9/9           c63_motion_estimate [4]
                0.00    0.13      30/30          dct_quantize [7]
                0.00    0.12      30/30          dequantize_idct [10]
                0.00    0.01      10/10          write_frame [20]
                0.00    0.00      10/10          destroy_frame [30]
                0.00    0.00      10/10          create_frame [29]
                0.00    0.00       9/9           c63_motion_compensate [40]
-----------------------------------------------
                                                 <spontaneous>
[2]    100.0    0.00    0.69                 main [2]
                0.00    0.69      10/10          c63_encode_image [1]
                0.00    0.00      10/10          read_yuv [33]
                0.00    0.00      10/10          get_dt [32]
                0.00    0.00       1/1           init_c63_enc [41]
                0.00    0.00       1/11          gettime [28]
-----------------------------------------------
                0.05    0.39   21384/21384       c63_motion_estimate [4]
[3]     63.8    0.05    0.39   21384         me_block_8x8 [3]
                0.39    0.00 15158016/15158016     sad_block_8x8_avx_v1 [5]
-----------------------------------------------
                0.00    0.44       9/9           c63_encode_image [1]
[4]     63.8    0.00    0.44       9         c63_motion_estimate [4]
                0.05    0.39   21384/21384       me_block_8x8 [3]
-----------------------------------------------
                0.39    0.00 15158016/15158016     me_block_8x8 [3]
[5]     56.5    0.39    0.00 15158016         sad_block_8x8_avx_v1 [5]
-----------------------------------------------
                0.01    0.12     720/720         dct_quantize [7]
[6]     18.1    0.01    0.12     720         dct_quantize_row [6]
                0.00    0.12   23760/23760       dct_quant_block_8x8 [8]
-----------------------------------------------
                0.00    0.13      30/30          c63_encode_image [1]
[7]     18.1    0.00    0.13      30         dct_quantize [7]
                0.01    0.12     720/720         dct_quantize_row [6]
-----------------------------------------------
                0.00    0.12   23760/23760       dct_quantize_row [6]
[8]     16.7    0.00    0.12   23760         dct_quant_block_8x8 [8]
                0.06    0.00   23760/23760       quantize_block_old [13]
                0.04    0.00  380160/380160      dct_1d_sse [14]
                0.01    0.00   23760/47520       scale_block_old [15]
                0.01    0.00   47520/95040       transpose_block_old [17]
-----------------------------------------------
                0.01    0.11     720/720         dequantize_idct [10]
[9]     16.7    0.01    0.11     720         dequantize_idct_row [9]
                0.01    0.10   23760/23760       dequant_idct_block_8x8 [11]
-----------------------------------------------
                0.00    0.12      30/30          c63_encode_image [1]
[10]    16.7    0.00    0.12      30         dequantize_idct [10]
                0.01    0.11     720/720         dequantize_idct_row [9]
-----------------------------------------------
                0.01    0.10   23760/23760       dequantize_idct_row [9]
[11]    15.2    0.01    0.10   23760         dequant_idct_block_8x8 [11]
                0.07    0.00   23760/23760       dequantize_block [12]
                0.01    0.00  380160/380160      idct_1d_sse [16]
                0.01    0.00   23760/47520       scale_block_old [15]
                0.01    0.00   47520/95040       transpose_block_old [17]
-----------------------------------------------
                0.07    0.00   23760/23760       dequant_idct_block_8x8 [11]
[12]    10.1    0.07    0.00   23760         dequantize_block [12]
-----------------------------------------------
                0.06    0.00   23760/23760       dct_quant_block_8x8 [8]
[13]     8.7    0.06    0.00   23760         quantize_block_old [13]
-----------------------------------------------
                0.04    0.00  380160/380160      dct_quant_block_8x8 [8]
[14]     5.8    0.04    0.00  380160         dct_1d_sse [14]
-----------------------------------------------
                0.01    0.00   23760/47520       dct_quant_block_8x8 [8]
                0.01    0.00   23760/47520       dequant_idct_block_8x8 [11]
[15]     2.9    0.02    0.00   47520         scale_block_old [15]
-----------------------------------------------
                0.01    0.00  380160/380160      dequant_idct_block_8x8 [11]
[16]     1.4    0.01    0.00  380160         idct_1d_sse [16]
-----------------------------------------------
                0.01    0.00   47520/95040       dct_quant_block_8x8 [8]
                0.01    0.00   47520/95040       dequant_idct_block_8x8 [11]
[17]     1.4    0.01    0.00   95040         transpose_block_old [17]
-----------------------------------------------
                0.01    0.00   23760/23760       write_interleaved_data_MCU [19]
[18]     1.4    0.01    0.00   23760         write_block [18]
                0.00    0.00  490521/490521      put_bits [22]
                0.00    0.00  210651/210651      bit_width [23]
-----------------------------------------------
                0.00    0.01   11880/11880       write_interleaved_data [21]
[19]     1.4    0.00    0.01   11880         write_interleaved_data_MCU [19]
                0.01    0.00   23760/23760       write_block [18]
-----------------------------------------------
                0.00    0.01      10/10          c63_encode_image [1]
[20]     1.4    0.00    0.01      10         write_frame [20]
                0.00    0.01      10/10          write_interleaved_data [21]
                0.00    0.00      10/10          write_SOI [38]
                0.00    0.00      10/10          write_SOF0 [37]
                0.00    0.00      10/10          write_DQT [35]
                0.00    0.00      10/10          write_DHT [34]
                0.00    0.00      10/10          write_SOS [39]
                0.00    0.00      10/10          write_EOI [36]
-----------------------------------------------
                0.00    0.01      10/10          write_frame [20]
[21]     1.4    0.00    0.01      10         write_interleaved_data [21]
                0.00    0.01   11880/11880       write_interleaved_data_MCU [19]
                0.00    0.00      10/10          flush_bits [31]
-----------------------------------------------
                0.00    0.00  490521/490521      write_block [18]
[22]     0.0    0.00    0.00  490521         put_bits [22]
                0.00    0.00  141628/142168      put_byte [24]
-----------------------------------------------
                0.00    0.00  210651/210651      write_block [18]
[23]     0.0    0.00    0.00  210651         bit_width [23]
-----------------------------------------------
                0.00    0.00      10/142168      flush_bits [31]
                0.00    0.00      20/142168      write_SOI [38]
                0.00    0.00      20/142168      write_EOI [36]
                0.00    0.00      40/142168      write_DHT_HTS [27]
                0.00    0.00      40/142168      write_DHT [34]
                0.00    0.00      70/142168      write_DQT [35]
                0.00    0.00     140/142168      write_SOS [39]
                0.00    0.00     200/142168      write_SOF0 [37]
                0.00    0.00  141628/142168      put_bits [22]
[24]     0.0    0.00    0.00  142168         put_byte [24]
-----------------------------------------------
                0.00    0.00   21384/21384       c63_motion_compensate [40]
[25]     0.0    0.00    0.00   21384         mc_block_8x8 [25]
-----------------------------------------------
                0.00    0.00      30/110         write_DQT [35]
                0.00    0.00      80/110         write_DHT_HTS [27]
[26]     0.0    0.00    0.00     110         put_bytes [26]
-----------------------------------------------
                0.00    0.00      40/40          write_DHT [34]
[27]     0.0    0.00    0.00      40         write_DHT_HTS [27]
                0.00    0.00      80/110         put_bytes [26]
                0.00    0.00      40/142168      put_byte [24]
-----------------------------------------------
                0.00    0.00       1/11          main [2]
                0.00    0.00      10/11          get_dt [32]
[28]     0.0    0.00    0.00      11         gettime [28]
-----------------------------------------------
                0.00    0.00      10/10          c63_encode_image [1]
[29]     0.0    0.00    0.00      10         create_frame [29]
-----------------------------------------------
                0.00    0.00      10/10          c63_encode_image [1]
[30]     0.0    0.00    0.00      10         destroy_frame [30]
-----------------------------------------------
                0.00    0.00      10/10          write_interleaved_data [21]
[31]     0.0    0.00    0.00      10         flush_bits [31]
                0.00    0.00      10/142168      put_byte [24]
-----------------------------------------------
                0.00    0.00      10/10          main [2]
[32]     0.0    0.00    0.00      10         get_dt [32]
                0.00    0.00      10/11          gettime [28]
-----------------------------------------------
                0.00    0.00      10/10          main [2]
[33]     0.0    0.00    0.00      10         read_yuv [33]
-----------------------------------------------
                0.00    0.00      10/10          write_frame [20]
[34]     0.0    0.00    0.00      10         write_DHT [34]
                0.00    0.00      40/142168      put_byte [24]
                0.00    0.00      40/40          write_DHT_HTS [27]
-----------------------------------------------
                0.00    0.00      10/10          write_frame [20]
[35]     0.0    0.00    0.00      10         write_DQT [35]
                0.00    0.00      70/142168      put_byte [24]
                0.00    0.00      30/110         put_bytes [26]
-----------------------------------------------
                0.00    0.00      10/10          write_frame [20]
[36]     0.0    0.00    0.00      10         write_EOI [36]
                0.00    0.00      20/142168      put_byte [24]
-----------------------------------------------
                0.00    0.00      10/10          write_frame [20]
[37]     0.0    0.00    0.00      10         write_SOF0 [37]
                0.00    0.00     200/142168      put_byte [24]
-----------------------------------------------
                0.00    0.00      10/10          write_frame [20]
[38]     0.0    0.00    0.00      10         write_SOI [38]
                0.00    0.00      20/142168      put_byte [24]
-----------------------------------------------
                0.00    0.00      10/10          write_frame [20]
[39]     0.0    0.00    0.00      10         write_SOS [39]
                0.00    0.00     140/142168      put_byte [24]
-----------------------------------------------
                0.00    0.00       9/9           c63_encode_image [1]
[40]     0.0    0.00    0.00       9         c63_motion_compensate [40]
                0.00    0.00   21384/21384       mc_block_8x8 [25]
-----------------------------------------------
                0.00    0.00       1/1           main [2]
[41]     0.0    0.00    0.00       1         init_c63_enc [41]
-----------------------------------------------

 This table describes the call tree of the program, and was sorted by
 the total amount of time spent in each function and its children.

 Each entry in this table consists of several lines.  The line with the
 index number at the left hand margin lists the current function.
 The lines above it list the functions that called this function,
 and the lines below it list the functions this one called.
 This line lists:
     index	A unique number given to each element of the table.
		Index numbers are sorted numerically.
		The index number is printed next to every function name so
		it is easier to look up where the function is in the table.

     % time	This is the percentage of the `total' time that was spent
		in this function and its children.  Note that due to
		different viewpoints, functions excluded by options, etc,
		these numbers will NOT add up to 100%.

     self	This is the total amount of time spent in this function.

     children	This is the total amount of time propagated into this
		function by its children.

     called	This is the number of times the function was called.
		If the function called itself recursively, the number
		only includes non-recursive calls, and is followed by
		a `+' and the number of recursive calls.

     name	The name of the current function.  The index number is
		printed after it.  If the function is a member of a
		cycle, the cycle number is printed between the
		function's name and the index number.


 For the function's parents, the fields have the following meanings:

     self	This is the amount of time that was propagated directly
		from the function into this parent.

     children	This is the amount of time that was propagated from
		the function's children into this parent.

     called	This is the number of times this parent called the
		function `/' the total number of times the function
		was called.  Recursive calls to the function are not
		included in the number after the `/'.

     name	This is the name of the parent.  The parent's index
		number is printed after it.  If the parent is a
		member of a cycle, the cycle number is printed between
		the name and the index number.

 If the parents of the function cannot be determined, the word
 `<spontaneous>' is printed in the `name' field, and all the other
 fields are blank.

 For the function's children, the fields have the following meanings:

     self	This is the amount of time that was propagated directly
		from the child into the function.

     children	This is the amount of time that was propagated from the
		child's children to the function.

     called	This is the number of times the function called
		this child `/' the total number of times the child
		was called.  Recursive calls by the child are not
		listed in the number after the `/'.

     name	This is the name of the child.  The child's index
		number is printed after it.  If the child is a
		member of a cycle, the cycle number is printed
		between the name and the index number.

 If there are any cycles (circles) in the call graph, there is an
 entry for the cycle-as-a-whole.  This entry shows who called the
 cycle (as parents) and the members of the cycle (as children.)
 The `+' recursive calls entry shows the number of function calls that
 were internal to the cycle, and the calls entry for each member shows,
 for that member, how many times it was called from other members of
 the cycle.

Copyright (C) 2012 Free Software Foundation, Inc.

Copying and distribution of this file, with or without modification,
are permitted in any medium without royalty provided the copyright
notice and this notice are preserved.

Index by function name

  [23] bit_width (c63_write.c) [31] flush_bits            [17] transpose_block_old (dsp.c)
   [1] c63_encode_image (c63enc.c) [32] get_dt            [34] write_DHT (c63_write.c)
  [40] c63_motion_compensate  [28] gettime                [27] write_DHT_HTS (c63_write.c)
   [4] c63_motion_estimate    [16] idct_1d_sse (dsp.c)    [35] write_DQT (c63_write.c)
  [29] create_frame           [41] init_c63_enc           [36] write_EOI (c63_write.c)
  [14] dct_1d_sse (dsp.c)     [25] mc_block_8x8 (me.c)    [37] write_SOF0 (c63_write.c)
   [8] dct_quant_block_8x8     [3] me_block_8x8 (me.c)    [38] write_SOI (c63_write.c)
   [7] dct_quantize           [22] put_bits               [39] write_SOS (c63_write.c)
   [6] dct_quantize_row       [24] put_byte               [18] write_block (c63_write.c)
  [11] dequant_idct_block_8x8 [26] put_bytes              [20] write_frame
  [12] dequantize_block (dsp.c) [13] quantize_block_old (dsp.c) [21] write_interleaved_data (c63_write.c)
  [10] dequantize_idct        [33] read_yuv (c63enc.c)    [19] write_interleaved_data_MCU (c63_write.c)
   [9] dequantize_idct_row     [5] sad_block_8x8_avx_v1
  [30] destroy_frame          [15] scale_block_old (dsp.c)
