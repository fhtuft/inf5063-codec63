140917-14-31-07

#define dct_1d                  dct_1d_sse
#define idct_1d                 idct_1d_sse
#define transpose_block         transpose_block_sse
#define scale_block             scale_block_sse
#define quantize_block          quantize_block_old
#define sad_block_8x8           sad_block_8x8_avx
#define me_block_8x8_sse        me_block_8x8

make[1]: Entering directory `/home/inf5063-g05/exam1/inf5063-codec63'
#./c63enc -w 352 -h 288 -o foreman -f 10 foreman_cif.yuv
./c63enc -w 352 -h 288 -o foreman.c63 -f 10 foreman.yuv
Limited to 10 frames.
Encoding frame 0, time ms: 27 Done!
Encoding frame 1, time ms: 59 Done!
Encoding frame 2, time ms: 59 Done!
Encoding frame 3, time ms: 59 Done!
Encoding frame 4, time ms: 59 Done!
Encoding frame 5, time ms: 59 Done!
Encoding frame 6, time ms: 59 Done!
Encoding frame 7, time ms: 59 Done!
Encoding frame 8, time ms: 59 Done!
Encoding frame 9, time ms: 59 Done!
make[1]: Leaving directory `/home/inf5063-g05/exam1/inf5063-codec63'

Flat profile:

Each sample counts as 0.01 seconds.
  %   cumulative   self              self     total           
 time   seconds   seconds    calls  ms/call  ms/call  name    
 72.23      0.39     0.39    21384     0.02     0.02  me_block_8x8
  7.41      0.43     0.04   380160     0.00     0.00  idct_1d_sse
  5.56      0.46     0.03   380160     0.00     0.00  dct_1d_sse
  1.85      0.47     0.01   490521     0.00     0.00  put_bits
  1.85      0.48     0.01    95040     0.00     0.00  transpose_block_sse
  1.85      0.49     0.01    23760     0.00     0.00  dct_quant_block_8x8
  1.85      0.50     0.01    23760     0.00     0.00  dequant_idct_block_8x8
  1.85      0.51     0.01    23760     0.00     0.00  dequantize_block
  1.85      0.52     0.01    23760     0.00     0.00  write_block
  1.85      0.53     0.01    21384     0.00     0.00  mc_block_8x8
  1.85      0.54     0.01      720     0.01     0.08  dct_quantize_row
  0.00      0.54     0.00   210651     0.00     0.00  bit_width
  0.00      0.54     0.00   142168     0.00     0.00  put_byte
  0.00      0.54     0.00    47520     0.00     0.00  scale_block_sse
  0.00      0.54     0.00    23760     0.00     0.00  quantize_block_old
  0.00      0.54     0.00    11880     0.00     0.00  write_interleaved_data_MCU
  0.00      0.54     0.00      720     0.00     0.09  dequantize_idct_row
  0.00      0.54     0.00      110     0.00     0.00  put_bytes
  0.00      0.54     0.00       40     0.00     0.00  write_DHT_HTS
  0.00      0.54     0.00       30     0.00     1.83  dct_quantize
  0.00      0.54     0.00       30     0.00     2.17  dequantize_idct
  0.00      0.54     0.00       11     0.00     0.00  gettime
  0.00      0.54     0.00       10     0.00    54.01  c63_encode_image
  0.00      0.54     0.00       10     0.00     0.00  create_frame
  0.00      0.54     0.00       10     0.00     0.00  destroy_frame
  0.00      0.54     0.00       10     0.00     0.00  flush_bits
  0.00      0.54     0.00       10     0.00     0.00  get_dt
  0.00      0.54     0.00       10     0.00     0.00  read_yuv
  0.00      0.54     0.00       10     0.00     0.00  write_DHT
  0.00      0.54     0.00       10     0.00     0.00  write_DQT
  0.00      0.54     0.00       10     0.00     0.00  write_EOI
  0.00      0.54     0.00       10     0.00     0.00  write_SOF0
  0.00      0.54     0.00       10     0.00     0.00  write_SOI
  0.00      0.54     0.00       10     0.00     0.00  write_SOS
  0.00      0.54     0.00       10     0.00     2.00  write_frame
  0.00      0.54     0.00       10     0.00     2.00  write_interleaved_data
  0.00      0.54     0.00        9     0.00     1.11  c63_motion_compensate
  0.00      0.54     0.00        9     0.00    43.34  c63_motion_estimate
  0.00      0.54     0.00        1     0.00     0.00  init_c63_enc

 %         the percentage of the total running time of the
time       program used by this function.

cumulative a running sum of the number of seconds accounted
 seconds   for by this function and those listed above it.

 self      the number of seconds accounted for by this
seconds    function alone.  This is the major sort for this
           listing.

calls      the number of times this function was invoked, if
           this function is profiled, else blank.
 
 self      the average number of milliseconds spent in this
ms/call    function per call, if this function is profiled,
	   else blank.

 total     the average number of milliseconds spent in this
ms/call    function and its descendents per call, if this 
	   function is profiled, else blank.

name       the name of the function.  This is the minor sort
           for this listing. The index shows the location of
	   the function in the gprof listing. If the index is
	   in parenthesis it shows where it would appear in
	   the gprof listing if it were to be printed.

Copyright (C) 2012 Free Software Foundation, Inc.

Copying and distribution of this file, with or without modification,
are permitted in any medium without royalty provided the copyright
notice and this notice are preserved.

		     Call graph (explanation follows)


granularity: each sample hit covers 2 byte(s) for 1.85% of 0.54 seconds

index % time    self  children    called     name
                0.00    0.54      10/10          main [2]
[1]    100.0    0.00    0.54      10         c63_encode_image [1]
                0.00    0.39       9/9           c63_motion_estimate [4]
                0.00    0.07      30/30          dequantize_idct [7]
                0.00    0.06      30/30          dct_quantize [9]
                0.00    0.02      10/10          write_frame [15]
                0.00    0.01       9/9           c63_motion_compensate [21]
                0.00    0.00      10/10          destroy_frame [30]
                0.00    0.00      10/10          create_frame [29]
-----------------------------------------------
                                                 <spontaneous>
[2]    100.0    0.00    0.54                 main [2]
                0.00    0.54      10/10          c63_encode_image [1]
                0.00    0.00      10/10          read_yuv [33]
                0.00    0.00      10/10          get_dt [32]
                0.00    0.00       1/1           init_c63_enc [40]
                0.00    0.00       1/11          gettime [28]
-----------------------------------------------
                0.39    0.00   21384/21384       c63_motion_estimate [4]
[3]     72.2    0.39    0.00   21384         me_block_8x8 [3]
-----------------------------------------------
                0.00    0.39       9/9           c63_encode_image [1]
[4]     72.2    0.00    0.39       9         c63_motion_estimate [4]
                0.39    0.00   21384/21384       me_block_8x8 [3]
-----------------------------------------------
                0.01    0.06   23760/23760       dequantize_idct_row [6]
[5]     12.0    0.01    0.06   23760         dequant_idct_block_8x8 [5]
                0.04    0.00  380160/380160      idct_1d_sse [11]
                0.01    0.00   23760/23760       dequantize_block [19]
                0.01    0.00   47520/95040       transpose_block_sse [18]
                0.00    0.00   23760/47520       scale_block_sse [24]
-----------------------------------------------
                0.00    0.07     720/720         dequantize_idct [7]
[6]     12.0    0.00    0.07     720         dequantize_idct_row [6]
                0.01    0.06   23760/23760       dequant_idct_block_8x8 [5]
-----------------------------------------------
                0.00    0.07      30/30          c63_encode_image [1]
[7]     12.0    0.00    0.07      30         dequantize_idct [7]
                0.00    0.07     720/720         dequantize_idct_row [6]
-----------------------------------------------
                0.01    0.05     720/720         dct_quantize [9]
[8]     10.2    0.01    0.05     720         dct_quantize_row [8]
                0.01    0.04   23760/23760       dct_quant_block_8x8 [10]
-----------------------------------------------
                0.00    0.06      30/30          c63_encode_image [1]
[9]     10.2    0.00    0.06      30         dct_quantize [9]
                0.01    0.05     720/720         dct_quantize_row [8]
-----------------------------------------------
                0.01    0.04   23760/23760       dct_quantize_row [8]
[10]     8.3    0.01    0.04   23760         dct_quant_block_8x8 [10]
                0.03    0.00  380160/380160      dct_1d_sse [12]
                0.01    0.00   47520/95040       transpose_block_sse [18]
                0.00    0.00   23760/47520       scale_block_sse [24]
                0.00    0.00   23760/23760       quantize_block_old [25]
-----------------------------------------------
                0.04    0.00  380160/380160      dequant_idct_block_8x8 [5]
[11]     7.4    0.04    0.00  380160         idct_1d_sse [11]
-----------------------------------------------
                0.03    0.00  380160/380160      dct_quant_block_8x8 [10]
[12]     5.6    0.03    0.00  380160         dct_1d_sse [12]
-----------------------------------------------
                0.01    0.01   23760/23760       write_interleaved_data_MCU [14]
[13]     3.7    0.01    0.01   23760         write_block [13]
                0.01    0.00  490521/490521      put_bits [17]
                0.00    0.00  210651/210651      bit_width [22]
-----------------------------------------------
                0.00    0.02   11880/11880       write_interleaved_data [16]
[14]     3.7    0.00    0.02   11880         write_interleaved_data_MCU [14]
                0.01    0.01   23760/23760       write_block [13]
-----------------------------------------------
                0.00    0.02      10/10          c63_encode_image [1]
[15]     3.7    0.00    0.02      10         write_frame [15]
                0.00    0.02      10/10          write_interleaved_data [16]
                0.00    0.00      10/10          write_SOI [38]
                0.00    0.00      10/10          write_SOF0 [37]
                0.00    0.00      10/10          write_DQT [35]
                0.00    0.00      10/10          write_DHT [34]
                0.00    0.00      10/10          write_SOS [39]
                0.00    0.00      10/10          write_EOI [36]
-----------------------------------------------
                0.00    0.02      10/10          write_frame [15]
[16]     3.7    0.00    0.02      10         write_interleaved_data [16]
                0.00    0.02   11880/11880       write_interleaved_data_MCU [14]
                0.00    0.00      10/10          flush_bits [31]
-----------------------------------------------
                0.01    0.00  490521/490521      write_block [13]
[17]     1.9    0.01    0.00  490521         put_bits [17]
                0.00    0.00  141628/142168      put_byte [23]
-----------------------------------------------
                0.01    0.00   47520/95040       dct_quant_block_8x8 [10]
                0.01    0.00   47520/95040       dequant_idct_block_8x8 [5]
[18]     1.9    0.01    0.00   95040         transpose_block_sse [18]
-----------------------------------------------
                0.01    0.00   23760/23760       dequant_idct_block_8x8 [5]
[19]     1.9    0.01    0.00   23760         dequantize_block [19]
-----------------------------------------------
                0.01    0.00   21384/21384       c63_motion_compensate [21]
[20]     1.9    0.01    0.00   21384         mc_block_8x8 [20]
-----------------------------------------------
                0.00    0.01       9/9           c63_encode_image [1]
[21]     1.9    0.00    0.01       9         c63_motion_compensate [21]
                0.01    0.00   21384/21384       mc_block_8x8 [20]
-----------------------------------------------
                0.00    0.00  210651/210651      write_block [13]
[22]     0.0    0.00    0.00  210651         bit_width [22]
-----------------------------------------------
                0.00    0.00      10/142168      flush_bits [31]
                0.00    0.00      20/142168      write_SOI [38]
                0.00    0.00      20/142168      write_EOI [36]
                0.00    0.00      40/142168      write_DHT_HTS [27]
                0.00    0.00      40/142168      write_DHT [34]
                0.00    0.00      70/142168      write_DQT [35]
                0.00    0.00     140/142168      write_SOS [39]
                0.00    0.00     200/142168      write_SOF0 [37]
                0.00    0.00  141628/142168      put_bits [17]
[23]     0.0    0.00    0.00  142168         put_byte [23]
-----------------------------------------------
                0.00    0.00   23760/47520       dct_quant_block_8x8 [10]
                0.00    0.00   23760/47520       dequant_idct_block_8x8 [5]
[24]     0.0    0.00    0.00   47520         scale_block_sse [24]
-----------------------------------------------
                0.00    0.00   23760/23760       dct_quant_block_8x8 [10]
[25]     0.0    0.00    0.00   23760         quantize_block_old [25]
-----------------------------------------------
                0.00    0.00      30/110         write_DQT [35]
                0.00    0.00      80/110         write_DHT_HTS [27]
[26]     0.0    0.00    0.00     110         put_bytes [26]
-----------------------------------------------
                0.00    0.00      40/40          write_DHT [34]
[27]     0.0    0.00    0.00      40         write_DHT_HTS [27]
                0.00    0.00      80/110         put_bytes [26]
                0.00    0.00      40/142168      put_byte [23]
-----------------------------------------------
                0.00    0.00       1/11          main [2]
                0.00    0.00      10/11          get_dt [32]
[28]     0.0    0.00    0.00      11         gettime [28]
-----------------------------------------------
                0.00    0.00      10/10          c63_encode_image [1]
[29]     0.0    0.00    0.00      10         create_frame [29]
-----------------------------------------------
                0.00    0.00      10/10          c63_encode_image [1]
[30]     0.0    0.00    0.00      10         destroy_frame [30]
-----------------------------------------------
                0.00    0.00      10/10          write_interleaved_data [16]
[31]     0.0    0.00    0.00      10         flush_bits [31]
                0.00    0.00      10/142168      put_byte [23]
-----------------------------------------------
                0.00    0.00      10/10          main [2]
[32]     0.0    0.00    0.00      10         get_dt [32]
                0.00    0.00      10/11          gettime [28]
-----------------------------------------------
                0.00    0.00      10/10          main [2]
[33]     0.0    0.00    0.00      10         read_yuv [33]
-----------------------------------------------
                0.00    0.00      10/10          write_frame [15]
[34]     0.0    0.00    0.00      10         write_DHT [34]
                0.00    0.00      40/142168      put_byte [23]
                0.00    0.00      40/40          write_DHT_HTS [27]
-----------------------------------------------
                0.00    0.00      10/10          write_frame [15]
[35]     0.0    0.00    0.00      10         write_DQT [35]
                0.00    0.00      70/142168      put_byte [23]
                0.00    0.00      30/110         put_bytes [26]
-----------------------------------------------
                0.00    0.00      10/10          write_frame [15]
[36]     0.0    0.00    0.00      10         write_EOI [36]
                0.00    0.00      20/142168      put_byte [23]
-----------------------------------------------
                0.00    0.00      10/10          write_frame [15]
[37]     0.0    0.00    0.00      10         write_SOF0 [37]
                0.00    0.00     200/142168      put_byte [23]
-----------------------------------------------
                0.00    0.00      10/10          write_frame [15]
[38]     0.0    0.00    0.00      10         write_SOI [38]
                0.00    0.00      20/142168      put_byte [23]
-----------------------------------------------
                0.00    0.00      10/10          write_frame [15]
[39]     0.0    0.00    0.00      10         write_SOS [39]
                0.00    0.00     140/142168      put_byte [23]
-----------------------------------------------
                0.00    0.00       1/1           main [2]
[40]     0.0    0.00    0.00       1         init_c63_enc [40]
-----------------------------------------------

 This table describes the call tree of the program, and was sorted by
 the total amount of time spent in each function and its children.

 Each entry in this table consists of several lines.  The line with the
 index number at the left hand margin lists the current function.
 The lines above it list the functions that called this function,
 and the lines below it list the functions this one called.
 This line lists:
     index	A unique number given to each element of the table.
		Index numbers are sorted numerically.
		The index number is printed next to every function name so
		it is easier to look up where the function is in the table.

     % time	This is the percentage of the `total' time that was spent
		in this function and its children.  Note that due to
		different viewpoints, functions excluded by options, etc,
		these numbers will NOT add up to 100%.

     self	This is the total amount of time spent in this function.

     children	This is the total amount of time propagated into this
		function by its children.

     called	This is the number of times the function was called.
		If the function called itself recursively, the number
		only includes non-recursive calls, and is followed by
		a `+' and the number of recursive calls.

     name	The name of the current function.  The index number is
		printed after it.  If the function is a member of a
		cycle, the cycle number is printed between the
		function's name and the index number.


 For the function's parents, the fields have the following meanings:

     self	This is the amount of time that was propagated directly
		from the function into this parent.

     children	This is the amount of time that was propagated from
		the function's children into this parent.

     called	This is the number of times this parent called the
		function `/' the total number of times the function
		was called.  Recursive calls to the function are not
		included in the number after the `/'.

     name	This is the name of the parent.  The parent's index
		number is printed after it.  If the parent is a
		member of a cycle, the cycle number is printed between
		the name and the index number.

 If the parents of the function cannot be determined, the word
 `<spontaneous>' is printed in the `name' field, and all the other
 fields are blank.

 For the function's children, the fields have the following meanings:

     self	This is the amount of time that was propagated directly
		from the child into the function.

     children	This is the amount of time that was propagated from the
		child's children to the function.

     called	This is the number of times the function called
		this child `/' the total number of times the child
		was called.  Recursive calls by the child are not
		listed in the number after the `/'.

     name	This is the name of the child.  The child's index
		number is printed after it.  If the child is a
		member of a cycle, the cycle number is printed
		between the name and the index number.

 If there are any cycles (circles) in the call graph, there is an
 entry for the cycle-as-a-whole.  This entry shows who called the
 cycle (as parents) and the members of the cycle (as children.)
 The `+' recursive calls entry shows the number of function calls that
 were internal to the cycle, and the calls entry for each member shows,
 for that member, how many times it was called from other members of
 the cycle.

Copyright (C) 2012 Free Software Foundation, Inc.

Copying and distribution of this file, with or without modification,
are permitted in any medium without royalty provided the copyright
notice and this notice are preserved.

Index by function name

  [22] bit_width (c63_write.c) [30] destroy_frame         [24] scale_block_sse (dsp.c)
   [1] c63_encode_image (c63enc.c) [31] flush_bits        [18] transpose_block_sse (dsp.c)
  [21] c63_motion_compensate  [32] get_dt                 [34] write_DHT (c63_write.c)
   [4] c63_motion_estimate    [28] gettime                [27] write_DHT_HTS (c63_write.c)
  [29] create_frame           [11] idct_1d_sse (dsp.c)    [35] write_DQT (c63_write.c)
  [12] dct_1d_sse (dsp.c)     [40] init_c63_enc           [36] write_EOI (c63_write.c)
  [10] dct_quant_block_8x8    [20] mc_block_8x8 (me.c)    [37] write_SOF0 (c63_write.c)
   [9] dct_quantize            [3] me_block_8x8 (me.c)    [38] write_SOI (c63_write.c)
   [8] dct_quantize_row       [17] put_bits               [39] write_SOS (c63_write.c)
   [5] dequant_idct_block_8x8 [23] put_byte               [13] write_block (c63_write.c)
  [19] dequantize_block (dsp.c) [26] put_bytes            [15] write_frame
   [7] dequantize_idct        [25] quantize_block_old (dsp.c) [16] write_interleaved_data (c63_write.c)
   [6] dequantize_idct_row    [33] read_yuv (c63enc.c)    [14] write_interleaved_data_MCU (c63_write.c)
