#include <stdio.h>
#include "../tables.h"

int main(){
    for (int row=0; row<8; row++){
        printf("{");
        for (int col=0; col<8; col++){
            printf("%ff, ", dctlookup[col][row]);
        }
        printf("},\n");
    }
    return 0;
}
